@ECHO OFF
::----------------------------------------------------------------------------------------------::
:: CREATE INSTALLER ISO FILE
:: Config by ID from this page
:: https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-professional?view=vs-2017
::----------------------------------------------------------------------------------------------::
SET ROOT_FOLDER=%~dp0
SET OUTPUT_FOLDER=%ROOT_FOLDER%..\VS_BuildTools_Installer

:: Select Visual Studio Version 
SET INSTALL_TOOLS_FILENAME=vs_BuildTools_15.9.exe
SET INSTALL_TOOLS=%ROOT_FOLDER%%INSTALL_TOOLS_FILENAME%
SET INSTALL_PARAMS=

::----------------------------------------------------------------------------------------------::
::------------------------------------------------------------- ---------------------------------::
:: .NET desktop build tools
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.ManagedDesktopBuildTools --includeRecommended


:: MSBuild Tools
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.MSBuildTools --includeRecommended


:: .NET Core build tools
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.NetCoreBuildTools --includeRecommended
:: .NET Core 2.0 development tools
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.Net.Core.Component.SDK


:: Universal Windows Platform build tools
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.UniversalBuildTools --includeRecommended
:: Windows SDK 16299
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.ComponentGroup.Windows10SDK.16299


:: Visual C++ build tools
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.VCTools --includeRecommended
:: Visual studio 2015 for desktop build tools
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Component.VC.140


:: Mobile Development with .NET
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.XamarinBuildTools --includeRecommended


:: .Net framework 4.7.1
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.Net.Component.4.7.1.SDK --add Microsoft.Net.Component.4.7.1.TargetingPack
::----------------------------------------------------------------------------------------------::
::----------------------------------------------------------------------------------------------::

SET INSTALL_PARAMS=%INSTALL_PARAMS% --lang en-US
SET INSTALL_CMD=%INSTALL_TOOLS% --layout %OUTPUT_FOLDER% %INSTALL_PARAMS%
CD /D %ROOT_FOLDER%
CALL %INSTALL_CMD%

::----------------------------------------------------------------------------------------------::
:: Write my info
(
ECHO Name: Tran Thanh Phong
ECHO Mail: phong.tranthanh@gameloft.component
)>"%OUTPUT_FOLDER%\MakeByPhongUs.txt"
::----------------------------------------------------------------------------------------------::
PAUSE