@ECHO OFF
::----------------------------------------------------------------------------------------------::
:: CREATE INSTALLER ISO FILE
:: Config by ID from this page
:: https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-professional?view=vs-2017
::----------------------------------------------------------------------------------------------::
SET ROOT_FOLDER=%~dp0
SET OUTPUT_FOLDER=%ROOT_FOLDER%..\VSInstaller

:: Select Visual Studio Version 
:: vs_Professional_15.9.exe
:: vs_Community_15.9.exe
SET INSTALL_TOOLS_FILENAME=vs_Community_15.9.exe
IF "%1" EQU "pro" SET INSTALL_TOOLS_FILENAME=vs_Professional_15.9.exe
SET INSTALL_TOOLS=%ROOT_FOLDER%%INSTALL_TOOLS_FILENAME%
SET INSTALL_PARAMS=

::----------------------------------------------------------------------------------------------::
::----------------------------------------------------------------------------------------------::
:: Visual Studio core editor
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Component.CoreEditor --includeRecommended


:: .NET desktop development
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.ManagedDesktop --includeRecommended
:: .Net core 2.0 development tool
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.Net.Core.Component.SDK


:: Desktop development with C++
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.NativeDesktop --includeRecommended


:: .NET Core cross-platform development
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.NetCoreTools --includeRecommended


:: Mobile development with .NET (Xamarin)
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.NetCrossPlat --includeRecommended


:: Universal Windows Platform development
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.Universal --includeRecommended
:: C++ Universal Windows Platform tools
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.ComponentGroup.UWP.VC


:: Python development
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Workload.Python --includeRecommended


:: Windows SDK 16299
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.ComponentGroup.Windows10SDK.16299


:: Visual studio 2015 for desktop build tools
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.VisualStudio.Component.VC.140


:: .Net framework 4.7.1
SET INSTALL_PARAMS=%INSTALL_PARAMS% --add Microsoft.Net.Component.4.7.1.SDK --add Microsoft.Net.Component.4.7.1.TargetingPack
::----------------------------------------------------------------------------------------------::
::----------------------------------------------------------------------------------------------::

SET INSTALL_PARAMS=%INSTALL_PARAMS% --lang en-US
SET INSTALL_CMD=%INSTALL_TOOLS% --layout %OUTPUT_FOLDER% %INSTALL_PARAMS%
CD /D %ROOT_FOLDER%
CALL %INSTALL_CMD%

::----------------------------------------------------------------------------------------------::
:: Write my info
(
ECHO Name: Tran Thanh Phong
ECHO Mail: phong.tranthanh@gameloft.component
)>"%OUTPUT_FOLDER%\MakeByPhongUs.txt"
::----------------------------------------------------------------------------------------------::
PAUSE